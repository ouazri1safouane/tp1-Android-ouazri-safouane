package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.myapplication.Country.Country;

public class DetailFragment extends Fragment {
    private Country[] tabcoun = Country.countries ;
    TextView title;
    EditText EditText;
    EditText EditText1;
    EditText EditText2;
    EditText EditText3;
    EditText EditText4;
    ImageView imageView;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.textView2);
        EditText = view.findViewById(R.id.f1);
        EditText1 = view.findViewById(R.id.f2);
        EditText2 = view.findViewById(R.id.f3);
        EditText3 = view.findViewById(R.id.f4);
        EditText4 = view.findViewById(R.id.f5);
        imageView = view.findViewById(R.id.imageView3);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
       title.setText(""+(tabcoun[args.getCountryId()].getName()));
        EditText.setText(""+(tabcoun[args.getCountryId()].getCapital()));
        EditText1.setText(""+(tabcoun[args.getCountryId()].getLanguage()));
        EditText2.setText(""+(tabcoun[args.getCountryId()].getCurrency()));
        EditText3.setText(""+tabcoun[args.getCountryId()].getPopulation());
        EditText4.setText(""+(tabcoun[args.getCountryId()].getArea()));
        String uri = tabcoun[args.getCountryId()].getImgUri();
        Context c = imageView.getContext();
        imageView.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}
