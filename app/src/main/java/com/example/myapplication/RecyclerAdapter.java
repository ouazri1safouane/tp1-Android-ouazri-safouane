package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Country.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private Country[] tabcoun = Country.countries ;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.itemTitle.setText(tabcoun[position].getName());
        holder.itemDetail.setText(tabcoun[position].getName());
        String uri = tabcoun[position].getImgUri();
        Context c = holder.itemImage.getContext();
        holder.itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));

    }

    @Override
    public int getItemCount() {
        return tabcoun.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemDetail;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);
            int position = getAdapterPosition();



            }
        }
    }








}